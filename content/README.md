---
description: Catalog of links to articles on web browser internals — JavaScript and JIT engined, DOM rendering, and more.
---

# Web Browsers Internals

This is a catalog of links to resources on web browser internals.

https://gitlab.com/Gerkis/web-browser-internals

## Author and contributors

Author of this project: Arthur ([ax330d](https://twitter.com/ax330d)) Gerkis.
