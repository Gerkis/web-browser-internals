---
description:
---

# Common to many browsers

| *Nr* | *URL* | *Description* | *Date* | *Author* | *Format* |
|  -- | -- | -- | -- | -- | -- |
| 1 | [https://www.html5rocks.com/en/tutorials/...](https://www.html5rocks.com/en/tutorials/internals/howbrowserswork/) | How Browsers Work: Behind the scenes of modern web browsers | 05-08-2011 | Tali Garsiel, Paul Irish | Article |
