---
description: Internals of Google Chrome, Google Chrome OS
---

# Google Chrome, Google Chrome OS

| *Nr* | *URL* | *Description* | *Date* | *Author* | *Format* |
|  -- | -- | -- | -- | -- | -- |
| 1 | [https://docs.google.com/document/d/1XyXV...](https://docs.google.com/document/d/1XyXV-wi4xnfnI5z_YjZXOIGQxxljR1g65tVub-tvkVo/edit) | How About JITting DOM Attribute Getters? | 06-11-2012 | Kentaro Hara | Document |
| 2 | [https://docs.google.com/document/d/1kOCU...](https://docs.google.com/document/d/1kOCUlJdh2WJMJGDf-WoEQhmnjKLaOYRbiHz5TiGJl14/edit?pli=1#) | Strings in Blink | 05-08-2013 | Adam Barth | Document |
| 3 | [https://docs.google.com/document/d/13cT9...](https://docs.google.com/document/d/13cT9Klgvt_ciAR3ONGvzKvw6fz9-f6E0FrqYFqfoc8Y/edit?pli=1) | Blink-in-JavaScript | 16-01-2014 | Kentaro Hara | Document |
| 4 | [https://docs.google.com/presentation/d/1...](https://docs.google.com/presentation/d/1gqK9F4lGAY3TZudAtdcxzMQNEE7PcuQrGu83No3l0lw/preview) | Headless Chrome | xx-xx-2017 | skyostil, alexclarke | Presentation |
| 5 | [https://docs.google.com/document/d/1aitS...](https://docs.google.com/document/d/1aitSOucL0VHZa9Z2vbRJSyAIsAz24kX8LFByQ5xQnUg/edit#heading=h.v5plba74lfde) | How Blink works | 14-08-2018 | haraken | Document |
| 6 | [https://developers.google.com/web/update...](https://developers.google.com/web/updates/2018/09/inside-browser-part1) | Inside look at modern web browser (part 1) | xx-09-2018 | Mariko Kosaka | Article |
| 7 | [https://developers.google.com/web/update...](https://developers.google.com/web/updates/2018/09/inside-browser-part2) | Inside look at modern web browser (part 2) | xx-09-2018 | Mariko Kosaka | Article |
