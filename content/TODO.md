# TODO

## Articles

[https://chromium.googlesource.com/chromium/src/+/master/third\_party/WebKit/Source/platform/heap/BlinkGCAPIReference.md](https://chromium.googlesource.com/chromium/src/+/master/third_party/WebKit/Source/platform/heap/BlinkGCAPIReference.md)

[https://struct.github.io/oilpan\_metadata.html](https://struct.github.io/oilpan_metadata.html)

[https://github.com/v8/v8/wiki](https://github.com/v8/v8/wiki)

[https://medium.com/@\_lrlna/garbage-collection-in-v8-an-illustrated-guide-d24a952ee3b8](https://medium.com/@_lrlna/garbage-collection-in-v8-an-illustrated-guide-d24a952ee3b8)

[https://github.com/a0viedo/demystifying-js-engines](https://github.com/a0viedo/demystifying-js-engines)

[https://developer.telerik.com/featured/a-guide-to-javascript-engines-for-idiots/](https://developer.telerik.com/featured/a-guide-to-javascript-engines-for-idiots/)

[https://wingolog.org/archives/2012/06/27/inside-javascriptcores-low-level-interpreter](https://wingolog.org/archives/2012/06/27/inside-javascriptcores-low-level-interpreter)

[https://docs.google.com/presentation/d/1V7gCqKR-edNdRDv0bDnJa\_uEs6iARAU2h5WhgxHyejQ/preview?slide=id.p](https://docs.google.com/presentation/d/1V7gCqKR-edNdRDv0bDnJa_uEs6iARAU2h5WhgxHyejQ/preview?slide=id.p)

[https://docs.google.com/presentation/d/1ak7YVrJITGXxqQ7tyRbwOuXB1dsLJlfpgC4wP7lykeo/preview?slide=id.p](https://docs.google.com/presentation/d/1ak7YVrJITGXxqQ7tyRbwOuXB1dsLJlfpgC4wP7lykeo/preview?slide=id.p)

[https://docs.google.com/document/d/1uDLu5uTRlCWePxQUEHc8yNQdEoE1BDISYdpggWEABnw](https://docs.google.com/document/d/1uDLu5uTRlCWePxQUEHc8yNQdEoE1BDISYdpggWEABnw)

[https://docs.google.com/document/d/1n7qYjQ5iy8xAkQVMYGqjIy\_AXu2\_JJtMoAcOOupO\_jQ/edit](https://docs.google.com/document/d/1n7qYjQ5iy8xAkQVMYGqjIy_AXu2_JJtMoAcOOupO_jQ/edit)

[https://docs.google.com/document/d/1gJDlk-9xkh6\_8M\_awrczWCaUuyr0Zd2TKjNBCiPO\_G4/edit](https://docs.google.com/document/d/1gJDlk-9xkh6_8M_awrczWCaUuyr0Zd2TKjNBCiPO_G4/edit)

[https://docs.google.com/document/d/1dkUXXmpJk7xBUeQM-olBpTHJ2MXamDgY\_kjNrl9JXMs/edit\#heading=h.swke19b7apg5](https://docs.google.com/document/d/1dkUXXmpJk7xBUeQM-olBpTHJ2MXamDgY_kjNrl9JXMs/edit#heading=h.swke19b7apg5)

[http://websrv0a.sdu.dk/ups/SCM/slides/lecture\_03\_mads\_ager.pdf](http://websrv0a.sdu.dk/ups/SCM/slides/lecture_03_mads_ager.pdf)

[https://downloads.contentful.com/nn534z2fqr9f/5CKHA7h43YKscaCGMQ0sO4/bc2c164a90b96ed0a5d887e11ae835b3/Melikhov\_Andrey\_V8\_under\_the\_hood\_\_1\_.pdf](https://downloads.contentful.com/nn534z2fqr9f/5CKHA7h43YKscaCGMQ0sO4/bc2c164a90b96ed0a5d887e11ae835b3/Melikhov_Andrey_V8_under_the_hood__1_.pdf)

[https://docs.google.com/document/d/13cT9Klgvt\_ciAR3ONGvzKvw6fz9-f6E0FrqYFqfoc8Y/edit?pli=1](https://docs.google.com/document/d/13cT9Klgvt_ciAR3ONGvzKvw6fz9-f6E0FrqYFqfoc8Y/edit?pli=1)

[https://docs.google.com/document/d/1kOCUlJdh2WJMJGDf-WoEQhmnjKLaOYRbiHz5TiGJl14/edit?pli=1\#](https://docs.google.com/document/d/1kOCUlJdh2WJMJGDf-WoEQhmnjKLaOYRbiHz5TiGJl14/edit?pli=1#)

[https://v8project.blogspot.bg/2017/05/launching-ignition-and-turbofan.html](https://v8project.blogspot.bg/2017/05/launching-ignition-and-turbofan.html)

[https://v8project.blogspot.de/2016/08/firing-up-ignition-interpreter.html](https://v8project.blogspot.de/2016/08/firing-up-ignition-interpreter.html)

[https://seclab.stanford.edu/websec/chromium/chromium-security-architecture.pdf](https://seclab.stanford.edu/websec/chromium/chromium-security-architecture.pdf)

[https://www.slideshare.net/nwind/virtual-machine-and-javascript-engine](https://www.slideshare.net/nwind/virtual-machine-and-javascript-engine)

[https://hitcon.org/2015/CMT/download/day2-h-r1.pdf](https://hitcon.org/2015/CMT/download/day2-h-r1.pdf)

[https://hacks.mozilla.org/2018/01/making-webassembly-even-faster-firefoxs-new-streaming-and-tiering-compiler/](https://hacks.mozilla.org/2018/01/making-webassembly-even-faster-firefoxs-new-streaming-and-tiering-compiler/)

[https://docs.google.com/presentation/d/1wiiZeRQp8-sXDB9xXBUAGbaQaWJC84M5RNxRyQuTmhk/edit\#slide=id.g1b891d63c6\_0\_0](https://docs.google.com/presentation/d/1wiiZeRQp8-sXDB9xXBUAGbaQaWJC84M5RNxRyQuTmhk/edit#slide=id.g1b891d63c6_0_0)

[https://docs.google.com/presentation/d/1XPu03ymz8W295mCftEC9KshH9Icxfq81YwIJQzQrvxo/edit\#slide=id.gbdc7c2c05\_0\_1052](https://docs.google.com/presentation/d/1XPu03ymz8W295mCftEC9KshH9Icxfq81YwIJQzQrvxo/edit#slide=id.gbdc7c2c05_0_1052)

[https://docs.google.com/presentation/d/1V7gCqKR-edNdRDv0bDnJa\_uEs6iARAU2h5WhgxHyejQ/edit\#slide=id.g1c8e1fcc0a\_0\_190](https://docs.google.com/presentation/d/1V7gCqKR-edNdRDv0bDnJa_uEs6iARAU2h5WhgxHyejQ/edit#slide=id.g1c8e1fcc0a_0_190)

https://mathiasbynens.be/notes/shapes-ics

https://blog.sessionstack.com/how-javascript-works-parsing-abstract-syntax-trees-asts-5-tips-on-how-to-minimize-parse-time-abfcf7e8a0c8

https://blog.sessionstack.com/how-javascript-works-the-rendering-engine-and-tips-to-optimize-its-performance-7b95553baeda


## Blogs

[http://darksi.de/](http://darksi.de/)

[http://mrale.ph/](http://mrale.ph/)

[http://benediktmeurer.de/archive/](http://benediktmeurer.de/archive/)

[https://hacks.mozilla.org/author/lclarkmozilla-com/](https://hacks.mozilla.org/author/lclarkmozilla-com/)
