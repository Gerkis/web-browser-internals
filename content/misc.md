---
description:
---

# Miscellaneous

| *Nr* | *URL* | *Description* | *Date* | *Author* | *Format* |
|  -- | -- | -- | -- | -- | -- |
| 1 | [* https://browser-security.x41-dsec.de/X41... *](https://browser-security.x41-dsec.de/X41-Browser-Security-White-Paper.pdf) | Browser Security White Paper | 19-09-2017 | Markus Vervier, Michele Orrù, Berend-Jan Wever, Eric Sesterhenn | Whitepaper |
| 2 | [https://cure53.de/#browser-security-whit...](https://cure53.de/#browser-security-whitepaper) | Cure53 Browser Security White Paper | 20-09-2017 | Mario Heiderich, Alex Inführ, Fabian Fäßler, Nikolai Krein, Masato Kinugawa, Filedescriptor, Dario Weißer | Whitepaper |
