---
description: Internals of Apple JavaScriptCore JavaScript engine
---

# Apple JavaScriptCore

| *Nr* | *URL* | *Description* | *Date* | *Author* | *Format* |
|  -- | -- | -- | -- | -- | -- |
| 1 | [https://www.bignerdranch.com/blog/javasc...](https://www.bignerdranch.com/blog/javascriptcore-example/) | JavaScriptCore by Example | 11-04-2014 | Joseph Dixon | Article |
| 2 | [https://webkit.org/blog/6411/javascriptcore-csi-a-crash-site-investigation-story/](https://webkit.org/blog/6411/javascriptcore-csi-a-crash-site-investigation-story/) | JavaScriptCore CSI: A Crash Site Investigation Story | 01-06-2016 | Mark Lam | Article |
| 3 | [http://phrack.org/papers/attacking_javas...](http://phrack.org/papers/attacking_javascript_engines.html) | Attacking JavaScript Engines: A case study of JavaScriptCore and CVE-2016-4622 | 27-10-2016 | saelo | Article |
