---
description: Internals of Microsoft Chakra JavaScript engine
---

# Microsoft Chakra

| _Nr_ | _URL_ | _Description_ | _Date_ | _Author_ | _Format_ |
| --- | --- | --- | --- | --- | --- |
| 1 | [https://github.com/Microsoft/ChakraCore/...](https://github.com/Microsoft/ChakraCore/wiki/Architecture-Overview) | Architecture Overview | 03-05-2017 | Microsoft | Article |
| 2 | https://www.microsoft.com/en-us/research/uploads/prod/2018/04/41159.compressed.pdf | Chakra: from script to optimized code | 04-2018 | Microsoft | Slides |



