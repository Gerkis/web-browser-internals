---
description: Internals of Google V8
---

# Google V8

| *Nr* | *URL* | *Description* | *Date* | *Author* | *Format* |
|  -- | -- | -- | -- | -- | -- |
| 1 | [https://medium.com/dailyjs/understanding...](https://medium.com/dailyjs/understanding-v8s-bytecode-317d46c94775) | Understanding V8’s Bytecode | 16-08-2017 | Franziska Hinkelmann | Article |
| 2 | [https://v8project.blogspot.com/2018/08/l...](https://v8project.blogspot.com/2018/08/liftoff.html) | Liftoff: a new baseline compiler for WebAssembly in V8 | 20-08-2018 | Clemens Hammacher | Article |
| 3 | [https://docs.google.com/presentation/d/1...](https://docs.google.com/presentation/d/1Z6oCocRASCfTqGq1GCo1jbULDGS-w-nzxkbVF7Up0u0/edit#slide=id.p) | Deoptimization in V8 | 09-12-2016 | Jaroslav Sevcik | Presentation |
